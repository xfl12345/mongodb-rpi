# Builder.
FROM arm64v8/debian:bullseye as build
ARG MONGO_VERSION="6.0.0"

# Install deps.
RUN dpkg --add-architecture arm64
RUN apt-get update && \
    apt-get install -y \
        gcc-aarch64-linux-gnu \
        g++-aarch64-linux-gnu \
        libssl-dev:arm64 \
        libcurl4-openssl-dev:arm64 \
        liblzma-dev:arm64 \
        python3 \
        python3-pip \
        python-dev-is-python3 \
        wget

# Build script.
RUN wget -O mongo.tar.gz https://github.com/mongodb/mongo/archive/refs/tags/r${MONGO_VERSION}.tar.gz
RUN tar -xvf mongo.tar.gz && rm mongo.tar.gz
WORKDIR /mongo-r${MONGO_VERSION}
RUN python3 -m pip install --user -r etc/pip/compile-requirements.txt
RUN python3 buildscripts/scons.py \
    MONGO_VERSION=${MONGO_VERSION} \
    CC=/usr/bin/aarch64-linux-gnu-gcc \
    CXX=/usr/bin/aarch64-linux-gnu-g++ \
    AR=/usr/bin/aarch64-linux-gnu-ar \
    OBJCOPY=/usr/bin/aarch64-linux-gnu-objcopy \
    STRIP=/usr/bin/aarch64-linux-gnu-strip \
    CCFLAGS="-march=armv8-a+crc -mtune=cortex-a72" \
    --disable-warnings-as-errors \
    --install-mode=hygienic \
    --install-action=hardlink \
    --separate-debug \
    archive-devcore
RUN mv build/opt/pkgs/devcore-runtime.tgz /devcore-runtime.tgz

# Runtime.
FROM arm64v8/ubuntu:focal

# Install curl.
RUN apt-get update && \
    apt-get install -y curl

# Extract mongodb from local file.
COPY --from=build /devcore-runtime.tgz /
RUN tar -xvf /devcore-runtime.tgz && \
    mv /bin /usr/local/bin && \
    rm /devcore-runtime.tgz

