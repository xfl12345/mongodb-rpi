FROM amd64/ubuntu:focal

# Download mongodb.
ARG DB_VER=mongodb-linux-x86_64-ubuntu2004-5.0.6
ARG DB_URL=https://fastdl.mongodb.org/linux/${DB_VER}.tgz

RUN apt-get update && \
    apt-get install -y curl && \
    curl -OL ${DB_URL} && \
    tar -zxvf ${DB_VER}.tgz && \
    mv /${DB_VER}/bin/* /usr/local/bin/ && \
    rm -rf /${DB_VER} ${DB_VER}.tgz

